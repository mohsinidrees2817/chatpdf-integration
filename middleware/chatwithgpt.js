const AWS = require("aws-sdk");
const axios = require("axios");
const fs = require("fs");
const FormData = require("form-data");
const path = require("path");

const {
  chatpdfConfig,
  region,
  accessKeyId,
  secretAccessKey,
} = require("../config/config");

const chatWityhGpt = async (req, res, next) => {
  const { message, sourceId } = req.body.data;

  console.log("User message:", message);
  console.log("Users file Source ID:", sourceId);

  const data = {
    sourceId: sourceId,
    messages: [
      {
        role: "user",
        content: message,
      },
    ],
  };
  axios
    .post("https://api.chatpdf.com/v1/chats/message", data, chatpdfConfig)
    .then((response) => {
      console.log("Message response by chatpdf API:", response.data);
      res.status(200).json({ content: response.data.content }); // Send error response back to the client
    })
    .catch((error) => {
      console.error("Error:", error.message);
      console.log("Response:", error.response.data);
      res.status(401).send(message);
    });
};

// file  upload via url

const fileUploadChatpdf = async (req, res, next) => {
  try {
    let url = await fetchFromS3();

    const data = {
      url: url,
    };
    console.log("Url provided to Chatpdf", url);
    axios
      .post("https://api.chatpdf.com/v1/sources/add-url", data, chatpdfConfig)
      .then((response) => {
        console.log(
          "Source ID (response by Chat pdf API ):",
          response.data.sourceId
        );
        res.status(200).json({ content: response.data.sourceId });
      })
      .catch((error) => {
        console.log("Error:", error.message);
        console.log("Response:", error.response.data);
      });
  } catch (error) {
    const message =
      (error.response && error.response.data && error.response.data.message) ||
      error.message ||
      error.toString();
    console.log("Error interacting with ChatPDF API:", message);
    res.status(401).send(message);
  }
};

const fetchFromS3 = async (req, res, next) => {
  const s3 = new AWS.S3({
    region: region,
    accessKeyId: accessKeyId,
    secretAccessKey: secretAccessKey,
  });
  try {
    const bucketName = "startupfile-aviram";
    const objectKey = "avram_file.pdf";
    const expirationInSeconds = 120; // URL expiration time in seconds (1 hour)

    const params = {
      Bucket: bucketName,
      Key: objectKey,
      Expires: expirationInSeconds,
    };

    const preSignedUrl = await s3.getSignedUrlPromise("getObject", params);
    console.log("url fetched from s3", preSignedUrl);
    return preSignedUrl;
  } catch (error) {
    console.error("Error generating pre-signed URL:", error);
    res.status(500).json({ message: "Error generating pre-signed URL" });
  }
};

const deletePDf = async (req, res, next) => {
  const data = {
    sources: ["src_Y6oRZQEtCHRRw6FgXOkkW"],
  };

  axios
    .post("https://api.chatpdf.com/v1/sources/delete", data, chatpdfConfig)
    .then((response) => {
      console.log("Success", response);
    })
    .catch((error) => {
      console.error("Error:", error.message);
      console.log("Response:", error.response.data);
    });
};
module.exports = {
  chatWityhGpt,
  fileUploadChatpdf,
  deletePDf,
};


//file upload from server //////////////////////////////////////////////////////////////////////////

// const fileUploadChatpdf = async (req, res, next) => {
//   const formData = new FormData();
//   const filePath = path.join(__dirname, "aviram_file.pdf");
//   formData.append("file", fs.createReadStream(filePath));
//   console.log("uploading to server");

//   axios
//     .post("https://api.chatpdf.com/v1/sources/add-file", formData, chatpdfConfig)
//     .then((response) => {
//       console.log("Source ID:", response.data.sourceId);
//     })
//     .catch((error) => {
//       console.log("Error:", error.message);
//       console.log("Response:", error.response.data);
//     });
// };
