const express = require("express");
const {
  chatWityhGpt,
  fileUploadChatpdf,
  deletePDf,
} = require("./middleware/chatwithgpt");
const cors = require("cors");

const app = express();
app.use(cors());

app.use(express.json());

app.post("/chat", chatWityhGpt, (req, res) => {
  return res.status(200).json({ message: "chat succeded" });
});

app.post("/fileupload", fileUploadChatpdf, (req, res) => {
  return res.status(200).json({ message: "file uploaded" });
});

app.delete("/delete", deletePDf, (req, res) => {
  return res.status(200).json({ message: "file deleted" });
});

const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`server started on port ${port}`));
