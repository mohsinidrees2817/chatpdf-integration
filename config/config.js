const chatpdfConfig = {
  headers: {
    "x-api-key": "sec_Ow2SSctaK88wwEr2iGQ3ayfmTcwGCjFi",
    "Content-Type": "application/json",
  },
};

const region = "";
const accessKeyId = "";
const secretAccessKey = "";

module.exports = {
  chatpdfConfig,
  region,
  accessKeyId,
  secretAccessKey,
};
